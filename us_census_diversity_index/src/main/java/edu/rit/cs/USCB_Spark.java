package edu.rit.cs;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import java.io.File;

/**
 * U.S. Census Diversity Index
 * Based on https://www2.census.gov/programs-surveys/popest/datasets/2010-2017/counties/asrh/cc-est2017-alldata.csv
 */
public class USCB_Spark
{
    public static final String OutputDirectory = "dataset/USCB-outputs";
    public static final String DatasetFile = "dataset/USCB.csv";

    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null)
            for (File file : allContents)
                deleteDirectory(file);
        return directoryToBeDeleted.delete();
    }

    public static void uscb(SparkSession spark) {
        // parse dataset file
        Dataset ds = spark.read()
                .option("header", "true")
                .option("sep", ",")
                .option("inferSchema", "true")
                .csv("us_census_diversity_index/" + DatasetFile);

        // encoders are created for Java beans
        Encoder<USCBPopulationStat> uscbEncoder = Encoders.bean(USCBPopulationStat.class);
        Dataset<USCBPopulationStat> ds1 = ds.as(uscbEncoder);
        // show initial table after import
        ds1.show();

        // filter and sum data across multiple years for all year groups
        Dataset ds2 = ds1.filter("AGEGRP = 0")
                .select("STNAME", "CTYNAME",
                "WA_MALE", "WA_FEMALE", "BA_MALE", "BA_FEMALE", "IA_MALE", "IA_FEMALE", "AA_MALE", "AA_FEMALE", "NA_MALE", "NA_FEMALE", "TOM_MALE", "TOM_FEMALE")
                .groupBy("STNAME", "CTYNAME")
                .sum();

        // You need to complete the rest

        // calculate the total number of males and females within each racial category in a city add a column for each
        Dataset sums = ds2.withColumn("WA",(ds2.col("sum(WA_MALE)").plus(ds2.col("sum(WA_FEMALE)"))))
                .withColumn("BA",(ds2.col("sum(BA_MALE)").plus(ds2.col("sum(BA_FEMALE)"))))
                .withColumn("IA", (ds2.col("sum(IA_MALE)").plus(ds2.col("sum(IA_FEMALE)"))))
                .withColumn("AA", (ds2.col("sum(AA_MALE)").plus(ds2.col("sum(AA_FEMALE)"))))
                .withColumn("NA", (ds2.col("sum(NA_MALE)").plus(ds2.col("sum(NA_FEMALE)"))))
                .withColumn("TOM", (ds2.col("sum(TOM_MALE)").plus(ds2.col("sum(TOM_FEMALE)"))));

        //show each table as it goes
        sums.show();

        // calculate population sum from each category
        Dataset totalSum = sums.withColumn("TOTAL", (sums.col("WA")
                .plus(sums.col("BA"))
                .plus(sums.col("IA"))
                .plus(sums.col("AA"))
                .plus(sums.col("NA"))
                .plus(sums.col("TOM"))));

        //show each table as it goes
        totalSum.show();

        // calculate the diversity index for each
        Dataset divIndex = totalSum.withColumn("Div Index",
                (((totalSum.col("WA").multiply(totalSum.col("TOTAL")))
                        .minus(totalSum.col("WA").multiply(totalSum.col("WA"))))
                        .plus((totalSum.col("BA").multiply(totalSum.col("TOTAL")))
                        .minus(totalSum.col("BA").multiply(totalSum.col("BA"))))
                        .plus(((totalSum.col("IA").multiply(totalSum.col("TOTAL")))
                        .minus(totalSum.col("IA").multiply(totalSum.col("IA")))))
                        .plus(((totalSum.col("AA").multiply(totalSum.col("TOTAL")))
                        .minus(totalSum.col("AA").multiply(totalSum.col("AA")))))
                        .plus(((totalSum.col("NA").multiply(totalSum.col("TOTAL")))
                        .minus(totalSum.col("NA").multiply(totalSum.col("NA")))))
                        .plus(((totalSum.col("TOM").multiply(totalSum.col("TOTAL")))
                        .minus(totalSum.col("TOM").multiply(totalSum.col("TOM")))))
                        .divide(totalSum.col("TOTAL").multiply(totalSum.col("TOTAL")))
                ));

        //show each table as it goes
        divIndex.show();


        // create the output data set with city and state name and diversity index
        Dataset output = divIndex.select("STNAME", "CTYNAME", "Div Index")
                .sort("STNAME", "CTYNAME")
                .withColumnRenamed("STNAME", "STATE")
                .withColumnRenamed("CTYNAME","CITY")
                .withColumnRenamed("Div Index","INDEX");


        //show this table and then convert it to a text file
        output.show();
        output.toJavaRDD().repartition(1).saveAsTextFile("us_census_diversity_index/dataset/output");

    }

    public static void main( String[] args )
    {
       // fill your code

        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[4]");
        sparkConf.setAppName("USCB data");

        //create a spark session to create Datasets and RDDs
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        //start the session
        uscb(spark);

        //close the session
        spark.close();
    }
}
